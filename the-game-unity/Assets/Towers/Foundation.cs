﻿using UnityEngine;

namespace Towers
{
    public class Foundation : MonoBehaviour, IFoundation
    {
        private ITower tower;
        
        public bool IsVacant()
        {
            return tower == null;
        }

        public void BuildTower(Tower selection)
        {
            if(!IsVacant())
                return;
            
            var mesh = GetComponent<MeshFilter>().mesh;
            var bounds = mesh.bounds;
            var position = new Vector3(0, bounds.max.y, 0);
            var instance = Instantiate(selection, transform);
            instance.transform.localPosition = position;
            tower = instance;
        }
    }
}