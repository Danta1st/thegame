﻿using UnityEngine;

namespace Towers
{
    public class Target : MonoBehaviour, ITarget
    {
        public Vector3 GetPosition()
        {
            return transform.position;
        }

        public void Hit()
        {
            Debug.Log($"{nameof(Target)} {nameof(Hit)}");
        }
    }
}