﻿using Tweening;
using UnityEngine;

namespace Towers
{
    public interface ITower
    {
        
    }
    
    [RequireComponent(typeof(SphereCollider))]
    public sealed class Tower : MonoBehaviour, ITower
    {
        public Projectile projectilePrefab;

        public Transform spawner;
        public float range = 3f;
        public int speed = 1;

        private readonly ITweener<float> timer = new FloatTweener().By(Easings.Linear);
        private ITarget target;

        private void Awake()
        {
            SetColliderRadius(range);
            timer.Over(1f / speed).OnComplete(Shoot);
        }

        private void SetColliderRadius(float radius)
        {
            var collider = GetComponent<SphereCollider>();
            collider.radius = radius;
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log($"{nameof(Tower)} {nameof(OnTriggerEnter)}: {other.name}");
            if (target != null)
                return;

            var collision = (ITarget) other.GetComponent<Target>();
            if(collision == null)
                return;

            target = collision;
            Shoot();
        }

        private void OnTriggerExit(Collider other)
        {
            Debug.Log($"{nameof(Tower)} {nameof(OnTriggerExit)}: {other.name}");
            if (target == null)
                return;

            var collision = (ITarget) other.GetComponent<Target>();
            if(collision == null)
                return;

            if (collision != target)
                return;
            
            target = null;
            timer.Stop();
        }

        private void Shoot()
        {
            Debug.Log($"{nameof(Shoot)}");
            var projectile = (IProjectile) Instantiate(projectilePrefab, spawner.position, spawner.rotation);
            projectile.SetTarget(target);
            
            timer.Start();
        }
    }
}