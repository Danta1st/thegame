﻿using UnityEngine;

namespace Towers
{
    public interface ITarget
    {
        Vector3 GetPosition();

        void Hit();
    }
}