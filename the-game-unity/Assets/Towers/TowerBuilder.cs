﻿using UnityEngine;

namespace Towers
{
    public class TowerBuilder : MonoBehaviour
    {
        public Tower towerPrefab;

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (!Physics.Raycast(ray, out var hit)) 
                    return;
                    
                var foundation = hit.transform.GetComponent<Foundation>();
                if (foundation != null)
                {
                    if(!foundation.IsVacant())
                        return;
                    
                    foundation.BuildTower(towerPrefab);
                }
            }
        }
    }
}