﻿namespace Towers
{
    public interface IFoundation
    {
        bool IsVacant();

        void BuildTower(Tower selection);
    }
}