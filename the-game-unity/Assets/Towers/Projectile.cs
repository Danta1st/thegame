﻿using Tweening;
using UnityEngine;

namespace Towers
{
    [RequireComponent(typeof(Rigidbody))]
    public class Projectile : MonoBehaviour, IProjectile
    {
        // private ITarget target;
        public float speed;
        public ITweener<Vector3> tweener = new Vector3Tweener();
        
        private void Awake()
        {
            tweener.Over(1f / speed).By(Easings.Linear).From(transform.position);
        }

        public void SetTarget(ITarget target)
        {
            // this.target = target;
            // var position = target.GetPosition();
            tweener
                .To(target.GetPosition())
                .OnProgress(vector3 => { transform.position = vector3; })
                .OnComplete(() => { Destroy(gameObject); })
                .Start();
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log($"{nameof(Projectile)} {nameof(OnTriggerEnter)}: {other.name}");
            var collision = (ITarget) other.GetComponent<Target>();
            collision?.Hit();

            Destroy(gameObject);
        }

        private void OnDestroy()
        {
            tweener.Stop();
        }
    }
}