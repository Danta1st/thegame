﻿namespace Towers
{
    public interface IProjectile
    {
        void SetTarget(ITarget target);
    }
}