﻿using UnityEngine;

namespace Attributes
{
    public class ObjectInstantiater : MonoBehaviour
    {
        public GameObject prefab;
        
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                Instantiate(prefab, Vector3.zero, Quaternion.identity);
            }
        }
    }
}