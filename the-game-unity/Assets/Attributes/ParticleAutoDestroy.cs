﻿using System.Collections;
using UnityEngine;

namespace Attributes
{
    public class ParticleAutoDestroy : MonoBehaviour
    {
        private ParticleSystem particles;

        private void Awake()
        {
            particles = GetComponent<ParticleSystem>();
            particles.Play();

            StartCoroutine(Wait());
        }

        private IEnumerator Wait()
        {
            yield return particles.isPlaying;
            
            Destroy(gameObject);
        }
    }
}
