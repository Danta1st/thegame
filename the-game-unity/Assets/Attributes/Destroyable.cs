﻿using Tweening;
using UnityEngine;

namespace Attributes
{
    public class Destroyable : MonoBehaviour
    {
        public GameObject particlesPrefab;
        
        private readonly Health health = new Health(new Range(0, 10));
        private readonly Tweener<float> tweener = new FloatTweener();

        private void Awake()
        {
            tweener
                .Over(0.2f)
                .By(Easings.CircIn)
                .OnProgress(f => { transform.localScale = Vector3.one * f; });

            var constitutionName = health.GetDisplayName();
            health.Increased += () =>
            {
                Debug.Log($"{constitutionName} Increased");
                tweener.From(1.5f).To(1f).Start();
            };
            health.Renewed += () => { Debug.Log($"{constitutionName} Renewed"); };

            health.Decreased += () =>
            {
                Debug.Log($"{constitutionName} Decreased");
                tweener.From(0.5f).To(1f).Start();
            };
            health.Depleted += () =>
            {
                Debug.Log($"{constitutionName} Depleted");
                Destroy();
            };
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                health.Add(1);
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                health.Subtract(1);
            }
        }

        private void Destroy()
        {
            Debug.Log($"{nameof(Destroy)}: {name}");

            Instantiate(particlesPrefab, transform.position, transform.rotation);
            
            tweener.Stop();
            Destroy(gameObject);
        }
    }
}