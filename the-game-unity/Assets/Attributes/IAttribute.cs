﻿namespace Attributes
{
    public interface IAttribute
    {
        string GetDisplayName();

        int GetValue();
        Range GetRange();

        void Add(int value);
        void Subtract(int value);
    }
}