﻿using System;

namespace Attributes
{
    public interface IAttributeReporter
    {
        event Action<int> Changed;
        
        event Action Increased;
        event Action Renewed;
        
        event Action Decreased;
        event Action Depleted;
    }
}