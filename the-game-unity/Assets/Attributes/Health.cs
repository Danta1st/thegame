﻿namespace Attributes
{
    public class Health : Attribute
    {
        public Health(Range range) : base(range.Max, range)
        {
        }
    }
}