﻿using System;
using UnityEngine;

namespace Attributes
{
    public abstract class Attribute : IAttribute, IAttributeReporter
    {
        public event Action<int> Changed;
        public event Action Increased;
        public event Action Renewed;
        public event Action Decreased;
        public event Action Depleted;
        
        private readonly string name;
        private int value;
        private Range range;

        protected Attribute(int value, Range range)
        {
            name = GetType().Name;
            this.range = range;
            this.value = Mathf.Clamp(value, range.Min, range.Max);
        }

        public string GetDisplayName()
        {
            return name;
        }

        public int GetValue()
        {
            return value;
        }

        public Range GetRange()
        {
            return range;
        }

        public virtual void Add(int value)
        {
            var change = this.value + value;
            OnUpdate(change);
        }

        public virtual void Subtract(int value)
        {
            var change = this.value - value;
            OnUpdate(change);
        }

        protected void OnUpdate(int value)
        {
            var clamped = Mathf.Clamp(value, range.Min, range.Max);
            if(clamped == this.value)
                return;

            //Decrease
            if (clamped < this.value)
            {
                Decreased?.Invoke();
                this.value = clamped;
                Changed?.Invoke(GetValue());

                if(this.value == range.Min)
                    Depleted?.Invoke();
                
                return;
            }

            //Increase
            if (clamped > this.value)
            {
                Increased?.Invoke();
                this.value = clamped;
                Changed?.Invoke(GetValue());
                
                if(this.value == range.Max)
                    Renewed?.Invoke();
            }
        }
    }
}