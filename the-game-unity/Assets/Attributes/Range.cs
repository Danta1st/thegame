﻿namespace Attributes
{
    public sealed class Range
    {
        public readonly int Min;
        public readonly int Max;

        public Range(int min, int max)
        {
            Min = min;
            Max = max;
        }
    }
}