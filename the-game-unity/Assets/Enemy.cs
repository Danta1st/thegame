﻿using System;
using Towers;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Target
{
    public event Action OnEnemyReachedDestination; 
    
    private NavMeshAgent agent;
    private Vector3 destination;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    public float Speed
    {
        get => agent.speed;
        set
        {
            if (value <= 0) throw new ArgumentOutOfRangeException(nameof(value));
            agent.speed = Speed;
        }
    }

    public void StartNavigation(Vector3 destination)
    {
        this.destination = destination;
        agent.SetDestination(destination);
        Debug.Log("Enemy destination: " + destination);
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, destination) < 0.3f)
        {
            OnEnemyReachedDestination?.Invoke();
            Destroy(gameObject);
        }
    }
}
