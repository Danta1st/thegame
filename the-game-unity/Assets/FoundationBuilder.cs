﻿using System.Collections.Generic;
using System.Linq;
using Attributes;
using UnityEngine;

public class FoundationBuilder : MonoBehaviour
{
    [SerializeField] private GameObject foundationPrefab;
    [SerializeField] private Transform floorTransform;
    
    private List<Vector3> occupiedPositions;

    private Vector3 startPosition;
    private Vector3 endPosition;

    private readonly IEnumerable<Vector3> directions = new List<Vector3>()
    {
        Vector3.forward,
        Vector3.back,
        Vector3.left,
        Vector3.right
    };

    private int width;
    private int length;

    public (Vector3 startPosition, Vector3 endPosition) Generate(int width, int length)
    {
        this.width = width;
        this.length = length;
        occupiedPositions = new List<Vector3>();

        GenerateStartAndEnd();
        TakeStep(startPosition, startPosition, directions);
        BuildFoundation();
        
        floorTransform.localScale = new Vector3(width, 1, length);

        return (transform.TransformPoint(startPosition), transform.TransformPoint(endPosition));
    }

    private void BuildFoundation()
    {
        for (var x = 0; x < width; x++)
        {
            for (var z = 0; z < length; z++)
            {
                if (occupiedPositions.Contains(new Vector3(x, 0, z)))
                    continue;

                var foundation = Instantiate(foundationPrefab, transform);
                foundation.transform.localPosition = new Vector3(x, 0, z);
            }
        }
    }

    private void GenerateStartAndEnd()
    {
        var xSegmentSize = Mathf.FloorToInt(width / 4f);
        var zSegmentSize = Mathf.FloorToInt(length);

        var startPositionX = Random.Range(0, xSegmentSize);
        var startPositionZ = Random.Range(0, zSegmentSize);
        startPosition = new Vector3(startPositionX, 0, startPositionZ);
        Debug.Log("start" + startPosition);

        var endPositionX = Random.Range(xSegmentSize * 3, xSegmentSize * 4);
        var endPositionZ = Random.Range(0, zSegmentSize);
        endPosition = new Vector3(endPositionX, 0, endPositionZ);
        Debug.Log("end" + endPosition);
    }

    private bool TakeStep(Vector3 previousPosition, Vector3 position, IEnumerable<Vector3> directionsToTry)
    {
        if (position == endPosition)
        {
            occupiedPositions.Add(position);
            return true;
        }

        if (!IsPositionValid(position) || GetsNewNeighbor(previousPosition, position))
            return false;

        occupiedPositions.Add(position);

        var directionsLeft = directionsToTry.Except(new[] {previousPosition - position}).ToList();
        
        bool done;
        do
        {
            if (!EndIsNeighbor(position, out var direction))
            {
                var randomIndex = Random.Range(0, directionsLeft.Count);
                direction = directionsLeft.ElementAt(randomIndex);
            }

            done = TakeStep(position, position + direction, directions);
            directionsLeft.Remove(direction);
        } while (directionsLeft.Any() && !done);

        if (done)
            return true;

        occupiedPositions.Remove(position);
        return false;
    }

    private bool IsPositionValid(Vector3 position)
    {
        if (position.x < 0 || position.x >= width)
            return false;
        
        //Ignore height, for now.
        
        if (position.z < 0 || position.z >= length)
            return false;
        
        if (occupiedPositions.Contains(position))
            return false;
        
        return true;
    }

    private bool GetsNewNeighbor(Vector3 previousPosition, Vector3 position)
    {
        return directions
            .Except(new[] {previousPosition - position})
            .Any(direction => occupiedPositions.Contains(position + direction));
    }

    private bool EndIsNeighbor(Vector3 position, out Vector3 directionToEnd)
    {
        foreach (var direction in directions)
        {
            if (position + direction != endPosition)
                continue;

            directionToEnd = direction;
            return true;
        }

        directionToEnd = Vector3.zero;
        return false;
    }
}