﻿using UnityEngine;
using UnityEngine.AI;
    
public class Mover : MonoBehaviour 
{
    private NavMeshAgent agent;

    public void StartNavigation(Vector3 destination)
    {
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(destination);
        Debug.Log("Enemy destination: " + destination);
    }
}