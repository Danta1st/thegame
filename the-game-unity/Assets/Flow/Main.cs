﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Flow
{
    public class Main : MonoBehaviour
    {
        [SerializeField] private FoundationBuilder foundationBuilder;
        [SerializeField] private Enemy enemyPrefab;
        private Vector3 startPosition;
        private Vector3 endPosition;
        private int difficulty;
        private int enemiesLeftInWave;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F12))
                Reload();

            if (enemiesLeftInWave == 0)
            {
                difficulty++;
                StartWave(difficulty);
            }
        }

        private void Awake()
        {
            var positions = foundationBuilder.Generate(16, 8);
            this.startPosition = positions.startPosition;
            this.endPosition = positions.endPosition;
        }

        public static void Reload()
        {
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }
        private void SpawnEnemy(int speed)
        {
            var enemy = Instantiate(enemyPrefab);
            enemy.Speed = speed;
            enemy.OnEnemyReachedDestination += EnemyReachedDestination;
            enemy.transform.position = startPosition;
            enemy.StartNavigation(endPosition);
            Debug.Log("spawn pos" + startPosition);
        }

        private void StartWave(int difficulty)
        {
            enemiesLeftInWave = difficulty * 5;
            for (var i = 0; i < enemiesLeftInWave; i++)
            {
                SpawnEnemy(difficulty);
            }
        }

        private void EnemyReachedDestination()
        {
            Debug.Log("Av!");
            enemiesLeftInWave--;
        }
    }
}
