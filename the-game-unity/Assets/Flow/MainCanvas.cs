﻿using UnityEngine;
using UnityEngine.UI;

namespace Flow
{
    public class MainCanvas : MonoBehaviour
    {
        //Money
        public Text money;

        //Wave
        public Text wave;

        //Lifes
        public LifeLeftDisplay lifeLeftDisplay;

        private void Awake()
        {
            //Update wave count
            
            //Update and spend money
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.DownArrow))
                lifeLeftDisplay.Decrease();
        }
    }
}