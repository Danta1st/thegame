﻿using Attributes;
using UnityEngine;
using UnityEngine.UI;

namespace Flow
{
    public class LifeLeftDisplay : MonoBehaviour
    {
        [Header("Dependencies")]
        public Text display;
        
        [Header("Settings")]
        public int maxHealth = 10;
        
        private Health health;

        private void Awake()
        {
            health = new Health(new Range(0, maxHealth));
            health.Changed += lifeCount =>
            {
                display.text = lifeCount.ToString();
            };
            
            health.Depleted += () =>
            {
                Debug.Log("Game Over");
                Main.Reload();
            };
        }

        public void Increase()
        {
            health.Add(1);
        }

        public void Decrease()
        {
            health.Subtract(1);
        }
    }
}